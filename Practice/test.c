#include <stdio.h>

int ilog2(int x) {
  printf("%d\n", x);
  int m16 = ((1<<16) + ~0) << 16; /* groups of 16 */
  printf("Groups of 16 = %x\n", m16);
  int m8 = (((1<<8) + ~0) << 24) + (((1<<8) + ~0) << 8); /* groups of 8 */
  printf("Groups of 8 = %x\n", m8);
  int m4 = (0xf0<<24) + (0xf0<<16) + (0xf0<<8) + 0xf0; /* groups of 4 */
  printf("Groups of 4 = %x\n", m4);
  int m2 = (0xcc<<24) + (0xcc<<16) + (0xcc<<8) + 0xcc; /* groups of 2 */
  printf("Groups of 2 = %x\n", m2);
  int m1 = (0xaa<<24) + (0xaa<<16) + (0xaa<<8) + 0xaa; /* groups of 1 */
  printf("Groups of 1 = %x\n", m1);
  int result = 0;
  int upper;
  int mask;
  /* m16 */
   printf("****FIRST SET****\n");
  upper = !!(x & m16);
  printf("Upper = %x\n", upper);
  result += upper << 4;
  printf("Result = %d\n", result);
  mask = m16 ^ ~((upper<<31)>>31);
  printf("Mask = %x\n\n", mask);
  /* m8 */
    
  printf("****SECOND SET****\n");
  upper = !!(x & m8 & mask);
  printf("Upper = %x\n", upper);
  result += upper << 3;
  printf("Result = %d\n", result);
  mask &= (m8 ^ ~((upper<<31)>>31));
  printf("Mask = %x\n\n", mask);
  /* m4 */
   printf("****THIRD SET****\n");  
  upper = !!(x & m4 & mask);
  printf("Upper = %x\n", upper);
  result += upper << 2;
  printf("Result = %d\n", result);
  mask &= (m4 ^ ~((upper<<31)>>31));
  printf("Mask = %x\n\n", mask);
  /* m2 */
  printf("****FOURTH SET****\n");  
  upper = !!(x & m2 & mask);
  printf("Upper = %x\n", upper);
  result += upper << 1;
  printf("Result = %d\n", result);
  mask &= (m2 ^ ~((upper<<31)>>31));
  printf("Mask = %x\n\n", mask);

  /* m1 */
  printf("****FIFTH SET****\n"); 
  upper = !!(x & m1 & mask);
  printf("Upper = %x\n", upper);
  result += upper;
  printf("Result = %d\n\n", result);
  return result;
}
int getByte(int x, int n) {

  /* Shift x n*8 positions right */
  int shift = n << 3;
  int xs = x >> shift;
  /* Mask byte */
  return xs & 0xFF;

}
int fitsBits(int x, int n) {
  printf("x = %x\n", x);
  int shift = 32 + ~n + 1;
  printf("%d\n", shift);
  int move = (x << shift) >> shift;
  printf("move = %x\n", move);
  printf("%x\n", !(x ^ move) );
  return !(x ^ move);
}
int bitCount(int x) {
    /* Sum 8 groups of 4 bits each */
    int m1 = 0x11 | (0x11 << 8);
    int mask = m1 | (m1 << 16);
    printf("%x\n", mask);
    int s = x & mask;
    printf("s = %x\n", s);
    s += x>>1 & mask;
    printf("s = %x\n", s);
    s += x>>2 & mask;
    printf("s = %x\n", s);
    s += x>>3 & mask;
    printf("s = %x\n", s);
    /* Now combine high and low order sums */
    s = s + (s >> 16);
    printf("After combining, s = %x\n", s);
    /* Low order 16 bits now consists of 4 sums,
       each ranging between 0 and 8.
       Split into two groups and sum */
    printf("*********\n");
    mask = 0xF | (0xF << 8);
    printf("mask = %x\n", mask);
    printf("(s & mask) = %x\n", (s & mask));
    printf("((s >> 4) & mask) = %x\n", ((s >> 4) & mask));
    s = (s & mask) + ((s >> 4) & mask);
    printf("Final s = %x\n", s);
    printf("(s + (s>>8)) = %x\n", (s + (s>>8)));
    return (s + (s>>8)) & 0x3F;
}

unsigned float_i2f(int x) {
  unsigned sign = (x < 0);
  unsigned ax = (x < 0) ? -x : x;
  unsigned exp = 127+31;
  unsigned residue;
  unsigned frac = 0;
  if (ax == 0) {
    exp = 0;
    frac = 0;
  } else {
    /* Normalize so that msb = 1 */
    while ((ax & (1<<31)) == 0) {
      ax = ax << 1;
      exp--;
    }
    /* Now have Bit 31 = MSB (becomes implied leading one)
       Bits 8-30 are tentative fraction,
       Bits 0-7 require rounding.
    */
    residue = ax & 0xFF;
    frac = (ax >> 8) & 0x7FFFFF; /* 23 bits */
    if (residue > 0x80 || (residue == 0x80 && (frac & 0x1))) {
      /* Round up */
      frac ++;
      /* Might need to renormalize */
      if (frac > 0x7FFFFF) {
 frac = (frac & 0x7FFFFF) >> 1;
 exp++;
      }
    }
  }
  return (sign << 31) | (exp << 23) | frac;
}

int divpwr2(int x, int n) {
    /* Handle rounding by generating bias:
       0 when x >= 0
       2^n-1 when x < 0
    */
    int mask = (1 << n) + ~0;
    int bias = (x >> 31) & mask;
    return (x+bias) >> n;
    
}

unsigned f2u(float f)
{
  union {
    unsigned u;
    float f;
  } v;
  v.u = 0;
  v.f = f;
  return v.u;
}

static float u2f(unsigned u)
{
  union {
    unsigned u;
    float f;
  } v;
  v.u = u;
  return v.f;
}

void printBits(unsigned int a){
    static int flag = 0;
    if(flag != 32) {
        ++flag;
        printBits(a/2);
        printf("%d ", a%2);
        --flag;
        if(flag == 31 || flag == 23) 
            putchar(' ');
    }
}
void printFloatBits(float x){
    unsigned int *iP = (unsigned int *)&x;
    printBits(*iP);
}
    
int main(){
  // printf("ANSWER=%x\t\n", float_i2f(4294967295));
   //printf("ANSWER=%x\t\n", float_i2f(25));
   //printf("%x", divpwr2(31));
   float val = 7;
   float val2 = 0.006;
   double val3 = 6E-6;
   double val4 = 6E-36;
   double val5 = 6E-38;
   double val6 = 6E-39; 
   double val7 = 6E-40;
   double val8 = 6E-41;
   printf ("f2u (%f) = %x = ", val, f2u(val));
   printFloatBits(val);
   printf ("\nf2u (%f) = %x = ", val2, f2u(val2));
   printFloatBits(val2);
   printf ("\nf2u (%lf) = %x = ", val3, f2u(val3));
   printFloatBits(val3);
   printf ("\nf2u (%lg) = %x = ", val4, f2u(val4));
   printFloatBits(val4);
   printf ("\nf2u (%lg) = %x = ", val5, f2u(val5));
   printFloatBits(val5);
   printf ("\nf2u (%lg) = %x = ", val6, f2u(val6));
   printFloatBits(val6);
   printf ("\nf2u (%lg) = %x = ", val7, f2u(val7));
   printFloatBits(val7);
   printf ("\nf2u (%lg) = %x = ", val8, f2u(val8));
   printFloatBits(val8);
   printf("\n");
    
   
}
