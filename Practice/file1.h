/*Definition as an array*/
char bread[5] = "abcde";

/*Definition as a character pointer*/
//char *bread = "abcde"; //Works only for a char pointer


/*NOTE DEFINITION OF A POINTER ALLOCATES SPACE ONLY WHEN ITS A CHAR POINTER*/

//int *x = 5;