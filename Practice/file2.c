/*
 * Playing w/ arrays
 * Ashish Kashinath
 */

#include <stdio.h>
#include <string.h>
#include "file1.h"

/* Irrespective of how we define, Compiler always rewrites to pointer to type
 * In other words, a copy of any array is never made since its highly inefficient
 * Imp Point is that since only a pointer is passed, the function never knows the size of the array
 * However this is not a problem in char arrays since we have NULL to indicate end of string
 */


/* All these three are equivalent */

void my_function(char *function_parameter){
    printf("----Definition contains pointer to char as argument\n");
    printf("----Length of array :%d\n", strlen(function_parameter));
}

/*void my_function(char function_parameter[]){
    printf("----Definition contains array of chars as argument\n");
    printf("----Length of array :%d\n", strlen(function_parameter));
}
*/

/*void my_function(char function_parameter[500]){
    printf("----Definition contains array of chars as argument w/ size also\n");
    printf("----Length of array :%d\n", strlen(function_parameter));
}
*/


int main(){
    /*Declaration as a pointer*/
    //extern char *bread;

    /*Declaration as a array */
    extern char bread[];
    extern int* x;
    
    //printf("Getting from another file the value pointed by x: %d", *x);

    int i;
    char *charp;
    
    /*Works irrespective of the style of declaration. Why? */
    printf("Length of the string: %x\n", strlen(bread));

    /*Modification works only when declared as an array */
    /*for (i=0;i<strlen(bread);i++){
        bread[i] = 'a';
    }*/
    

    charp = bread;
    
    /*Use Array name in an Expression: Compiler converts it into a pointer based expression */
    for (i=0;i<strlen(bread);i++){
        printf("%c ", bread[i]);// *(bread+i*sizeof(each element))
        printf("%c ", i[bread]);// Never used in production code.
        printf("%c ", charp[i]);
        printf("%c \n", *(charp+i));
    }

    my_function(bread);
    /*
    for (i=0;i<strlen(bread);i++){
        printf("%c ", bread[i]);
    }
    */

    /*Array names are treated as pointers in function calls */
    /*
    for (i=0;i<5;i++)
        printf("Array at location %x holds string %s\n", bread, bread);
    */
    printf("\n");

}

